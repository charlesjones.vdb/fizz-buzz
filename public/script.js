
//FIZZ BUZZ GAME

function endResult() {
    var gameRange = Number(document.getElementById("gameRange").value) ,
        buzz = Number(document.getElementById("bInput").value) ,
        fizz = Number(document.getElementById("fInput").value) ;

    // Clears all previous list elements
    var ul = document.getElementById("gameOutput") ;
    while(ul.firstChild) ul.removeChild(ul.firstChild) ;

    for(let i=0; i<gameRange; i++) {
        result = gameLogic(i+1, fizz, buzz) ;
        let liLast = document.createElement("li") ;
        liLast.innerHTML = result ;
        liLast.classList.add("list-group-item") ;
        gameOutput.append(liLast) ;
    }
}

function gameLogic(i, fizz, buzz) {
	let f = ((i % fizz) === 0 ) ;
    let b = ((i % 10) === buzz ) ;
  
    if (f && b) { 
    return "FizzBuzz" ; 
    } else if (f) { 
        return "Fizz" ; 
    } else if (b) { 
    return "Buzz" ; 
    } return i ;
}

//CHECKBOX TEST

//CHECKBOX IN THE HEADER TO IMPACT ALL OTHER CHECKBOXES
function checkAll(status) {
    var input = document.querySelectorAll("input.rowCheckbox"); 
	if (status.checked) {
      for (let i=0; i<input.length; i++) {
        input[i].checked = true;
      }
    } else {
      for (let i=0; i<input.length; i++) {
        input[i].checked = false;         
      }  
    }
}


//Required for all below functions
const table = document.getElementById("checkboxTable");



//ADDING A NEW ROW TO THE TABLE
function addRow () {
    var newRow = table.insertRow(-1);  //adding a row to the end of the table
    
    let cell1 = newRow.insertCell(0); //adding the first cell into the new row
    newChkBox = document.createElement("input"); 
    newChkBox.type = "checkbox";
    newChkBox.classList.add("rowCheckbox");
    cell1.scope = "row";
    cell1.classList.add("text-center");
    cell1.appendChild(newChkBox);

    let cell2 = newRow.insertCell(1);
    cell2.innerHTML = "This";

    let cell3 = newRow.insertCell(2);
    cell3.innerHTML = "That"; 
}

//To be used for Deleting & Reseting
function rowCount(table) {
    return table.rows.length;
}

//DELETING A ROW FROM THE TABLE
function deleteRow () {
    for (var i=1; i<rowCount(table); i++) {
        var row = table.rows[i];
        var chkbox = row.cells[0].childNodes[0];

        if (chkbox.checked == true) {
            row.style.display = "none";
        }
    }
}

//RESETTING THE TABLE
function resetRows () {
    for (var i=1; i<rowCount(table); i++) {
        table.rows[i].style.display = "";
    }
}
